// Package tmpl handles loading and rendering HTML templates.
//
// Primarily this package wraps the "html/template" package but adds support for
// base templates, internationalization, and live-reloading in dev-mode.
//
// # Base Templates
//
// To use base templates first define them separately from your standard
// templates. In each one define sections to override, for example, a base
// template that constructs a simple page might look like this:
//
//	{{ define "base" }}
//	<!doctype HTML>
//	<html>
//		<h1>{{ define "title" }}</h1>
//	</html>
//	{{ end }}
//
// Then in your other pages you can re-define "title" and call base:
//
//	{{ template "base" . }}
//	{{ block "title" }}My Page{{ end }}
package tmpl // import "code.soquee.net/tmpl"

import (
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/xsrftoken"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/message/catalog"
)

// Option is used to configure a template.
type Option func(*Template)

// Dev returns an option that enables live-reloading of templates.
//
// This is most often used in conjunction with a build directive that embeds the
// templates in production mode, or uses the local filesystem in dev mode.
func Dev(dev bool) Option {
	return func(t *Template) {
		t.dev = dev
	}
}

// FS is an option that loads all files in the given filesystem as individual
// templates.
func FS(vfs fs.FS) Option {
	return func(t *Template) {
		oldParse := t.parseFuncs
		t.parseFuncs = func(t Template) error {
			if oldParse != nil {
				err := oldParse(t)
				if err != nil {
					return err
				}
			}

			// Load individual templates separately.
			return fs.WalkDir(vfs, ".", func(fpath string, d fs.DirEntry, err error) error {
				// Recurse into directories, but don't try to read them as files.
				if d != nil && d.IsDir() {
					return nil
				}
				contents, err := fs.ReadFile(vfs, fpath)
				if err != nil {
					return err
				}
				newTmpl, err := template.Must(t.baseTmpl.Clone()).New(fpath).Parse(string(contents))
				if err != nil {
					return err
				}
				// Load both the template from the file itself as well as any
				// subtemplates that were created in the file into the template map.
				// Similarly named sub-templates will overwrite each other just like
				// calling one of the builtin templates loaders, but we also have the
				// ability to load a base template properly and just not render
				// templates that are re-used by the base directly (instead we'd load
				// the file template that calls the base template and re-defines
				// sections of it).
				for _, subt := range newTmpl.Templates() {
					name := subt.Name()
					if name == "" {
						continue
					}
					t.tmpls[name] = newTmpl
				}
				return nil
			})
		}
	}
}

// BaseFS is an option that loads all files in the given filesystem as base
// layout templates that are available for other templates to call.
func BaseFS(vfs fs.FS) Option {
	return func(t *Template) {
		oldParse := t.baseParseFuncs
		t.baseParseFuncs = func(t Template) error {
			if oldParse != nil {
				err := oldParse(t)
				if err != nil {
					return err
				}
			}

			// Load base templates all at once and parse them in the context of any
			// existing base templates.
			if t.baseTmpl == nil {
				t.baseTmpl = template.New("").Funcs(t.funcs)
			}
			var err error
			t.baseTmpl, err = t.baseTmpl.ParseFS(vfs, "*")
			if err != nil {
				return err
			}
			t.tmpls[""] = t.baseTmpl
			return nil
		}
	}
}

// Catalog gives the template access to the provided catalog of translations.
func Catalog(c catalog.Catalog) Option {
	return func(t *Template) {
		t.cat = c
	}
}

// Funcs adds the elements of the argument map to the templates function map.
func Funcs(f template.FuncMap) Option {
	return func(t *Template) {
		t.funcs = f
	}
}

// Template wraps an "html/template".Template and adds better support for base
// templates, internationalization, and live reloading functionality for easy
// development.
type Template struct {
	cat   catalog.Catalog
	funcs template.FuncMap
	dev   bool

	parseFuncs     func(Template) error
	baseParseFuncs func(Template) error
	baseTmpl       *template.Template
	tmpls          map[string]*template.Template
}

// New creates a new undefined template with the given options.
func New(opt ...Option) (Template, error) {
	t := Template{}
	for _, o := range opt {
		o(&t)
	}
	return t.parse()
}

// parse loads the templates from the filesystem and parses them.
// For more information and options see the Template type.
func (t Template) parse() (Template, error) {
	t.tmpls = make(map[string]*template.Template)
	t.baseTmpl = template.New("").Funcs(t.funcs)
	t.tmpls[""] = t.baseTmpl
	if t.baseParseFuncs != nil {
		err := t.baseParseFuncs(t)
		if err != nil {
			return t, err
		}
	}
	if t.parseFuncs != nil {
		err := t.parseFuncs(t)
		if err != nil {
			return t, err
		}
	}
	return t, nil
}

// Execute executes the template, reloading it first if we're in dev mode.
func (t Template) Execute(wr io.Writer, data interface{}) error {
	var err error
	if t.dev {
		t, err = t.parse()
		if err != nil {
			return err
		}
	}
	return t.tmpls[""].Execute(wr, data)
}

// Templates returns a slice of all templates in the list.
//
// If we're in dev-mode and an error is encountered while reloading it is
// silently ignored and a nil slice is returned.
func (t Template) Templates() []*template.Template {
	if t.dev {
		var err error
		t, err = t.parse()
		if err != nil {
			return nil
		}
	}

	tt := make([]*template.Template, 0)
	for _, tmpl := range t.tmpls {
		tt = append(tt, tmpl)
	}
	return tt
}

// ExecuteTemplate executes the named template, reloading it if we're in dev
// mode.
func (t Template) ExecuteTemplate(wr io.Writer, name string, data interface{}) error {
	var err error
	if t.dev {
		t, err = t.parse()
		if err != nil {
			return err
		}
	}
	tmpl, ok := t.tmpls[name]
	if !ok {
		return fmt.Errorf("no such template %q", name)
	}
	return tmpl.ExecuteTemplate(wr, name, data)
}

// FlashType is the type of a flash message.
type FlashType string

// A list of flash types.
const (
	FlashDanger  FlashType = "danger"
	FlashSuccess FlashType = "success"
	FlashWarn    FlashType = "warn"
)

const (
	flashCookie = "flash"
)

// SetFlash sets a flash message using a cookie.
// Flash messages can also be set when rendering the response, but since this
// will not work for redirects or methods without a response, sometimes we need
// to set a cookie and read the value from there.
func SetFlash(w http.ResponseWriter, flash Flash) {
	http.SetCookie(w, &http.Cookie{
		Name:   flashCookie,
		Value:  string(flash.Type) + ":" + flash.Message,
		Path:   "/",
		Secure: true,
	})
}

// Flash is a flash message which may be used to convey information to the user.
type Flash struct {
	Message string
	Type    FlashType
}

// Page represents data that can apply generally to any page.
type Page struct {
	Path    string
	URL     *url.URL
	Domain  string
	Host    string
	XSRF    string
	Lang    language.Tag
	Printer *message.Printer
	Flash   Flash
	UID     int

	// Data may be set by a template renderer when the template is executed
	// and should not be set by callers of this package (except by setting the
	// extraData parameters on a template renderer).
	// It will contain data that can only be known at render time and not when the
	// renderer is constructed (which may or may not be the same).
	Data interface{}
}

// T attempts to translate the string "s" using p.Printer.
func (p Page) T(key message.Reference, a ...interface{}) string {
	return p.Printer.Sprintf(key, a...)
}

func defaultData(p Page) interface{} {
	return p
}

// RenderFunc is the type used to render templates into pages.
// For more information see Renderer.
type RenderFunc func(uid int, flash Flash, w http.ResponseWriter, r *http.Request, extraData interface{}) error

// Renderer returns a function that can be used to render pages using the
// provided templates.
//
// The data function is used to construct the data passed to the template (which
// should embed the provided Page).
// If it is nil, the page is used.
// If xsrfKey is provided, an XSRF token is constructed and passed to the page.
// If a flash message is passed to the returned function, it is displayed
// immediately and overrides any flash message set in a cookie (without clearing
// the cookie).
// To set a flash message in a cookie (eg. to persist it across a redirect) see
// SetFlash.
func Renderer(domain, xsrfKey, tmplName string, tmpls Template, data func(Page) interface{}) RenderFunc {
	if data == nil {
		data = defaultData
	}
	matcher := tmpls.cat.Matcher()

	return func(uid int, flash Flash, w http.ResponseWriter, r *http.Request, extraData interface{}) error {
		var xsrfToken string
		if xsrfKey != "" {
			xsrfToken = xsrftoken.Generate(xsrfKey, strconv.Itoa(uid), r.URL.Path)
		}

		if flash.Message == "" {
			cookie, err := r.Cookie(flashCookie)
			const (
				successPrefix = string(FlashSuccess) + ":"
				dangerPrefix  = string(FlashDanger) + ":"
				warnPrefix    = string(FlashWarn) + ":"
			)
			if err == nil && cookie != nil && cookie.Value != "" {
				switch {
				case strings.HasPrefix(cookie.Value, successPrefix):
					flash.Message = cookie.Value[len(successPrefix):]
					flash.Type = FlashSuccess
				case strings.HasPrefix(cookie.Value, dangerPrefix):
					flash.Message = cookie.Value[len(dangerPrefix):]
					flash.Type = FlashDanger
				case strings.HasPrefix(cookie.Value, warnPrefix):
					flash.Message = cookie.Value[len(warnPrefix):]
					flash.Type = FlashWarn
				}
				// Clear the flash message.
				http.SetCookie(w, &http.Cookie{
					Name:    flashCookie,
					Expires: time.Now().Add(-100 * time.Hour),
					MaxAge:  -1,
					Path:    "/",
					Secure:  true,
				})
			}
		}

		/* #nosec */
		acceptLangs, _, _ := language.ParseAcceptLanguage(r.Header.Get("Accept-Language"))
		tag, _, _ := matcher.Match(acceptLangs...)

		return tmpls.ExecuteTemplate(w, tmplName, data(Page{
			Path:    r.URL.Path,
			URL:     r.URL,
			Host:    r.Host,
			Domain:  domain,
			XSRF:    xsrfToken,
			Flash:   flash,
			UID:     uid,
			Data:    extraData,
			Lang:    tag,
			Printer: message.NewPrinter(tag, message.Catalog(tmpls.cat)),
		}))
	}
}
