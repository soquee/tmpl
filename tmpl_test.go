package tmpl_test

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"testing"
	"testing/fstest"

	"golang.org/x/net/xsrftoken"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/message/catalog"

	"code.soquee.net/tmpl"
)

var anyErr = errors.New("tmpl_test: any error allowed")

var tmplFuncs = map[string]interface{}{
	"q": func(s string) string {
		return strconv.Quote(s)
	},
}

func mapFS(m map[string]string) fstest.MapFS {
	mapFS := make(fstest.MapFS)
	for name, contents := range m {
		mapFS[name] = &fstest.MapFile{
			Data: []byte(contents),
		}
	}
	return mapFS
}

func testTmpls() map[string]string {
	return map[string]string{
		"basic.tmpl": `{{define "basic"}}<a>*Test&*</a>{{ end }}`,
		"q.tmpl":     `<div>{{q "test"}}</div>`,
		"foo.tmpl":   `{{template "base.tmpl" .}}{{define "content"}}Foo{{end}}`,
		"bar.tmpl":   `{{template "base.tmpl" .}}{{define "content"}}Bar{{end}}`,
	}
}

func testBaseTmpls() map[string]string {
	return map[string]string{
		"base.tmpl": `<body>{{template "content" .}}</body>`,
	}
}

var testAssets = map[string]string{
	"test": "test",
}

var tmplTests = [...]struct {
	tmpls  map[string]string
	assets map[string]string
	dev    bool
	render string
	data   interface{}
	out    string
	err    error
	base   string
}{
	0: {
		// No such directory "/" in an empty mapFS.
		// This behavior isn't relied upon, but it may be good to know if it changes
		// so test it.
		err: os.ErrNotExist,
	},
	1: {
		// There is always a default template and files that don't end in ".tmpl"
		// are not loaded.
		tmpls: map[string]string{
			"test.nope": "not a template",
		},
		render: "notvalid",
		err:    anyErr,
	},
	2: {
		// The root template is called "" and is a fragment that can't be
		// rendered.
		tmpls:  testTmpls(),
		render: "",
		err:    anyErr,
	},
	3: {
		// Test rendering a simple template
		tmpls:  testTmpls(),
		render: "basic",
		out:    "<a>*Test&*</a>",
	},
	4: {
		// Test custom function
		tmpls:  testTmpls(),
		render: "q.tmpl",
		out:    `<div>&#34;test&#34;</div>`,
	},
	5: {
		// Test base template rendering.
		tmpls:  testTmpls(),
		render: "foo.tmpl",
		out:    "<body>Foo</body>",
	},
	6: {
		tmpls:  testTmpls(),
		render: "bar.tmpl",
		out:    "<body>Bar</body>",
	},
}

func TestTemplates(t *testing.T) {
	for i, tc := range tmplTests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			tmplsfs := mapFS(tc.tmpls)
			basefs := mapFS(testBaseTmpls())
			tmpls, err := tmpl.New(
				tmpl.FS(tmplsfs),
				tmpl.BaseFS(basefs),
				tmpl.Dev(tc.dev),
				tmpl.Catalog(message.DefaultCatalog),
				tmpl.Funcs(tmplFuncs),
			)
			switch {
			case err != nil && !errors.Is(err, tc.err) && tc.err != anyErr:
				t.Fatalf("Wrong error on parse: want=%v, got=%v", tc.err, err)
			case err != nil:
				return
			}
			if len(tmpls.Templates()) < 1 {
				t.Errorf("There should always be a default template")
			}
			buf := &bytes.Buffer{}
			if tc.render == "" {
				return
			}
			err = tmpls.ExecuteTemplate(buf, tc.render, tc.data)
			switch {
			case err != nil && err != tc.err && tc.err != anyErr:
				t.Fatalf("Wrong error on execute: want=%v, got=%v", tc.err, err)
			case err == nil && tc.err != nil:
				t.Fatalf("Expected error: %q", tc.err)
			case err != nil && tc.err == anyErr:
				return
			}
			if s := buf.String(); s != tc.out {
				t.Errorf("Wrong output:\nwant=%q,\n got=%q", tc.out, s)
			}

			// Try again but mutating the template string. Only in dev mode should this have
			// any effect.
			buf.Reset()
			tc.tmpls[tc.render+".tmpl"] = "changed"
			err = tmpls.ExecuteTemplate(buf, tc.render, tc.data)
			switch {
			case err != nil && err != tc.err && tc.err != anyErr:
				t.Fatalf("Wrong error on rerender: want=%v, got=%v", tc.err, err)
			case err != nil:
				return
			}
			if tc.dev {
				if s := buf.String(); s != "changed" {
					t.Errorf("Wrong output on rerender:\nwant=%q,\ngot=%q", "changed", s)
				}
			} else {
				if s := buf.String(); s != tc.out {
					t.Errorf("Wrong output on rerender:\nwant=%q,\ngot=%q", tc.out, s)
				}
			}

			buf.Reset()
			err = tmpls.Execute(buf, tc.data)
			if s := buf.String(); s != "" {
				t.Errorf("Execute should not return any data: %q", s)
			}
			if err == nil {
				t.Errorf("Execute should always error")
			}
		})
	}
}

func renderTmpls() map[string]string {
	return map[string]string{
		"localize_nums.tmpl": "{{.Printer.Sprint 100000}}",
		"page_lang.tmpl":     "{{.Lang}}",
		"show_flash.tmpl":    "{{.Flash.Type}}: {{.Flash.Message}}",
		"xsrf.tmpl":          `{{define "xsrf"}}{{.XSRF}}{{end}}`,
		"domain.tmpl":        `{{define "domain"}}{{.Domain}}{{end}}`,
		"translate.tmpl":     `{{define "translate"}}{{ .T "spring time" }}{{end}}`,
	}
}

var renderTests = [...]struct {
	header      http.Header
	tmplName    string
	out         string
	code        int
	data        func(tmpl.Page) interface{}
	xsrfKey     string
	flash       tmpl.Flash
	cookieFlash tmpl.Flash
	extra       interface{}
	wantErr     bool
	catalog     catalog.Catalog
}{
	0: {wantErr: true /* undefined template */},
	1: {tmplName: "localize_nums.tmpl", out: "100,000"},
	2: {tmplName: "page_lang.tmpl", out: "und"},
	3: {
		// Does setting a flash at render time work.
		tmplName: "show_flash.tmpl",
		flash:    tmpl.Flash{Type: tmpl.FlashDanger, Message: "A flash msg"},
		out:      "danger: A flash msg",
	},
	4: {
		// Does setting a cookie based flash work.
		tmplName:    "show_flash.tmpl",
		cookieFlash: tmpl.Flash{Type: tmpl.FlashSuccess, Message: "cookie flash"},
		out:         "success: cookie flash",
	},
	5: {
		tmplName:    "show_flash.tmpl",
		flash:       tmpl.Flash{Type: tmpl.FlashDanger, Message: "render flash"},
		cookieFlash: tmpl.Flash{Type: tmpl.FlashWarn, Message: "cookie flash"},
		out:         "danger: render flash",
	},
	6: {
		tmplName:    "show_flash.tmpl",
		cookieFlash: tmpl.Flash{Type: tmpl.FlashDanger, Message: "cookie flash"},
		out:         "danger: cookie flash",
	},
	7: {
		tmplName:    "show_flash.tmpl",
		cookieFlash: tmpl.Flash{Type: tmpl.FlashWarn, Message: "cookie flash"},
		out:         "warn: cookie flash",
	},
	8:  {tmplName: "xsrf", xsrfKey: "0000"},
	9:  {tmplName: "xsrf"},
	10: {tmplName: "domain", out: "example.org"},
	11: {
		header: map[string][]string{"Accept-Language": {"es-MX"}},
		catalog: func() catalog.Catalog {
			b := catalog.NewBuilder()
			b.Set(language.LatinAmericanSpanish, "yes", catalog.String("si"))
			b.Set(language.Spanish, "yes", catalog.String("si"))
			return b
		}(),
		tmplName: "page_lang.tmpl",
		out:      "es-MX",
	},
	12: {
		header: map[string][]string{"Accept-Language": {"la", "en-US"}},
		catalog: func() catalog.Catalog {
			b := catalog.NewBuilder()
			b.Set(language.English, "spring time", catalog.String("spring time"))
			b.Set(language.Make("la"), "spring time", catalog.String("tempus vernum"))
			return b
		}(),
		tmplName: "translate",
		out:      "tempus vernum",
	},
	13: {
		header: map[string][]string{"Accept-Language": {"en-US"}},
		catalog: func() catalog.Catalog {
			b := catalog.NewBuilder()
			b.Set(language.English, "spring time", catalog.String("spring time"))
			b.Set(language.Make("la"), "spring time", catalog.String("tempus vernum"))
			return b
		}(),
		tmplName: "translate",
		out:      "spring time",
	},
}

func TestRenderer(t *testing.T) {
	tmplsfs := mapFS(renderTmpls())
	const (
		uid = 1253
	)

	for i, tc := range renderTests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			if tc.catalog == nil {
				tc.catalog = message.DefaultCatalog
			}
			tmpls, err := tmpl.New(
				tmpl.FS(tmplsfs),
				tmpl.Catalog(tc.catalog),
				tmpl.Funcs(tmplFuncs),
			)
			if err != nil {
				t.Fatalf("Unexpected error creating templates: %q", err)
			}
			render := tmpl.Renderer(
				"example.org", tc.xsrfKey, tc.tmplName, tmpls, tc.data)

			w := httptest.NewRecorder()

			r, err := http.NewRequest("GET", "/", nil)
			if err != nil {
				t.Fatalf("Unexpected error creating request: %q", err)
			}
			r.Header = tc.header
			if r.Header == nil {
				r.Header = make(http.Header)
			}
			// This is a bit jank, but we're only creating this response so that we
			// can get a serialized copy of the cookie to add to the request.
			// I'm not sure why you'd ever need to do this outside of tests, but it
			// may indicate that an API change is in order.
			if tc.cookieFlash.Message != "" {
				cookieStore := httptest.NewRecorder()
				tmpl.SetFlash(cookieStore, tc.cookieFlash)
				for _, c := range cookieStore.Result().Cookies() {
					r.AddCookie(c)
				}
			}

			err = render(uid, tc.flash, w, r, tc.data)

			// If no flash message was set, the cookie one should have been cleared
			// (otherwise it should still exist).
			if tc.flash.Message == "" {
				cookies := w.Result().Cookies()
				if tc.cookieFlash.Message != "" && (len(cookies) == 0 || cookies[0].MaxAge != -1) {
					t.Errorf("Expected clear flash cookie to be set")
				}
			} else {
				if len(w.Result().Cookies()) > 0 {
					t.Error("Did not expect clear flash cookie to be set")
				}
			}

			switch {
			case tc.wantErr && err == nil:
				t.Errorf("Expected an error but got none")
			case !tc.wantErr && err != nil:
				t.Errorf("Unexpected error logged: %q", err)
			}
			// We know net/http will always return 200 if no code is set, so just make
			// that the default.
			if tc.code == 0 {
				tc.code = 200
			}
			if w.Code != tc.code {
				t.Errorf("Unexpected code: want=%d, got=%d", tc.code, w.Code)
			}

			if tc.tmplName == "xsrf" && tc.xsrfKey != "" {
				if s := w.Body.String(); !xsrftoken.Valid(s, tc.xsrfKey, strconv.Itoa(uid), "/") {
					t.Errorf("Generated xsrf token did not validate!")
				}
			} else {
				if s := w.Body.String(); s != tc.out {
					t.Errorf("Unexpected body: want=%q, got=%q", tc.out, s)
				}
			}

			// func(uid int, flash Flash, w http.ResponseWriter, r *http.Request, tc.extra) {
		})
	}
}

func TestMultiLoad(t *testing.T) {
	// Test that when loading multiple filesystems the old templates aren't
	// overwritten (ie. the render func saves and calls the old render func and
	// returns the error if one exists).
	firstMap := map[string]string{
		"old": `OLD`,
	}
	secondMap := map[string]string{
		"newOK": `NEW`,
	}
	tmpls, err := tmpl.New(
		tmpl.FS(mapFS(firstMap)),
		tmpl.FS(mapFS(secondMap)),
	)
	if err != nil {
		t.Errorf("unexpected error parsing templates: %v", err)
	}
	buf := &bytes.Buffer{}
	err = tmpls.ExecuteTemplate(buf, "old", nil)
	if err != nil {
		t.Errorf("expected old template to render, got error: %v", err)
	}
	err = tmpls.ExecuteTemplate(buf, "newOK", nil)
	if err != nil {
		t.Errorf("expected new template to render, got error: %v", err)
	}
	const expected = `OLDNEW`
	if s := buf.String(); s != expected {
		t.Errorf("unexpected output, want=%q, got=%q", expected, s)
	}

	firstMap["broken"] = `BROKEN {{-}}`
	_, err = tmpl.New(
		tmpl.FS(mapFS(firstMap)),
		tmpl.FS(mapFS(secondMap)),
	)
	if err == nil {
		t.Errorf("expected error parsing original broken templates: %v", err)
	}

	_, err = tmpl.New(
		tmpl.BaseFS(mapFS(firstMap)),
		tmpl.BaseFS(mapFS(secondMap)),
	)
	if err == nil {
		t.Errorf("expected error parsing original broken base templates: %v", err)
	}
}
