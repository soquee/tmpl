module code.soquee.net/tmpl

go 1.21

require (
	golang.org/x/net v0.19.0
	golang.org/x/text v0.14.0
)

retract (
	v0.0.6 // Published as part of the old package, before the split.
	v0.0.5 // Published as part of the old package, before the split.
	v0.0.4 // Published as part of the old package, before the split.
)
