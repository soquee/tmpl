# tmpl

Package **`tmpl`** handles loading and rendering HTML templates.

```go
import (
	"code.soquee.net/tmpl"
)
```


## License

The package may be used under the terms of the BSD 2-Clause License a copy of
which may be found in the [`LICENSE`] file.

Unless you explicitly state otherwise, any contribution submitted for inclusion
in the work by you shall be licensed as above, without any additional terms or
conditions.

[`LICENSE`]: ./LICENSE
